from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import Context, RequestContext, loader
from django.shortcuts import render_to_response, get_object_or_404
from django.template.loader import render_to_string

from django.db import models
from django.contrib.auth.models import User
from webapp.models import *

from django.contrib.auth import authenticate as auth_func
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from django.contrib import messages
from django import forms

def home(request):
	return render_to_response('index.html',{}, RequestContext(request))


