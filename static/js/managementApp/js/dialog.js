function CustomAlert(){
    this.render = function(dialog,id){
        var winW = window.innerWidth;
        var winH = window.innerHeight;
        var dialogoverlay = document.getElementById('dialogoverlay');
        var dialogbox = document.getElementById('dialogbox');
        dialogoverlay.style.display = "block";
        dialogoverlay.style.height = winH+"px";
        dialogbox.style.left = (winW/2) - (550 * .5)+"px";
        dialogbox.style.top = "100px";
        dialogbox.style.display = "block";
        document.getElementById('dialogboxhead').innerHTML = "Confirmation";
        document.getElementById('dialogboxbody').innerHTML = dialog;
        $('#dialogboxfoot').html('<button class="btn btn-danger" onclick="Alert.yes(' + id + ')">Yes</button> <button class="btn btn-primary" onclick="Alert.no()">No</button>');
        // document.getElementById('dialogboxfoot').innerHTML = '<button class="btn btn-primary" onclick="Alert.yes(' + id + ')">Yes</button> <button class="btn btn-primary" onclick="Alert.no()">No</button>';
    }
	this.no = function(){
		document.getElementById('dialogbox').style.display = "none";
		document.getElementById('dialogoverlay').style.display = "none";
	}
	this.yes = function(id){
        $.ajax({
            url: "/management/users/",
            method:"POST",
            data: 'user_id='+id+'&type=delete',
            success: function(result){
               window.location = window.location;
               // $("#users").html(result);
            },
        });
	    document.getElementById('dialogbox').style.display = "none";
	    document.getElementById('dialogoverlay').style.display = "none";
//	}
};
}
var Alert = new CustomAlert();
//security!
//needed so that our ajax doesn't get killed by django
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
//please let my ajax in!
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});