var display_num = 0;


function newDisplay(argument) {
    //get arguments to build display
    display = {}

    //create a table or a graph
    if ($('#tableSelect').is(':visible')) {


        display["pgn"] = parseInt($("#gidTable").val());

        if (display["pgn"]==null) {
            console.log("TODO error here")
        };

        display["spns"] = [];
        var count = 0;
        $('#spn_selectTable').children('input').each(function (index) {
            if (this.checked && count < 3) {
                //alert("value is" + this.value + "checked?" + this.checked);
                display["spns"][count] = parseInt(this.value);
                count++;
            }
        });

        display["displaySA"] = true;
        display["displayPriority"] = true;
        display["timestamp"] = true;

        createTable();
    }
    else{


        display["pgn"] = parseInt($("#gidGraph").val());

        if (display["pgn"]==null) {
            console.log("TODO error here")
        };

        var count = 0;
        display["spns"] = [];
        $('#spn_selectGraph').children('input').each(function (index) {
            if (this.checked && count < 3) {
                display["spns"][count] = parseInt(this.value);
                count++;
            }
        });

        if ($("#timestampTrue").prop("checked", true)) {
            display["timestamp"] = true;
        }else{
            display["timestamp"] = false;
        }

        display["displaySA"] = false;
        display["displayPriority"] = false;

        createGraph()
    }



    jsonObj["displays"].push(display)

    console.log("The pushed graph is")
    console.log(JSON.stringify(display))
    console.log(jsonObj)

    graphSel = true;

    changeButtonState();
}

function createTable(){
    //TODO
    var table =  { 
      "id":1,
      "type":"table",
      "datasets":[],
      "table_ref": {},
    } 

    table['id'] = parseInt($("#gidTable").val())

    var count = 0;
    $('#spn_selectTable').children('input').each(function (index) {
        if (this.checked) {
            table["datasets"][count] = parseInt(this.value);
            count++;
        }
    });

    var tableId = "myTable" + display_num;

    //get title value or else just call it chart 1 2 ..
    var title_text = $("#tableTitleInput").val()
    $("#tableTitleInput").val("")
    if (title_text == undefined ^ title_text == "") {title_text="Display " + display_num}
    title_text += (" For PGN:" + table['id'])

   
   //set up for the table
   var text = "<div class=\"col-md-6\"><div class=\"panel panel-default\"><div class=\"panel-heading\"> "
   text += title_text;
   text += "</div><div style=\"max-height: 400px;overflow-y: auto;overflow-x: auto;\"><table class=\"table\"><thead><tr>"
   //add the headers to the table
   text += "<th>Time</th><th>Source Address</th><th>Priority</th>"
   //next need to add the SPNs
   
   

   for (var i = 0; i < count; i++) {
       var loc = $.inArray(table["datasets"][i],currentPGN['spns'])
       if (loc == -1) {alert("error")}
       var currLb = currentPGN['labels'][loc];
        text += "<th>"+currLb + "</th>"
   }

   //now that all the headers are added we can add rest
   text += "</tr></thead>";
   //add the id of the table so we can find it
   text += "<tbody id=\"" + tableId + "\">";
   //close out

   //attach methods to the table_ref to add data!!
   //method addData("timestamp","sourceaddr","priority",[array of spn vals])
   table["table_ref"] = new Table(count,tableId)

   text += "</tr></tbody></table></div></div></div>";

   $("#graphs").append(text);

    //add to global array
    displays.push(table)
    display_num++;
}
function createGraph() {
    //create object to hold ref to this new graph
    var graph =  { 
      "id":1,
      "type": "graph",
      "datasets":[],
      "chart_ref": {},

    }


    
    var options = JSON.parse(JSON.stringify(template_options));
    //don't include everything just yet.
    var data = JSON.parse(JSON.stringify(template_data));


    //number of datasets and their ids
    //also add to our graph object
    //TODO add datasets of spns
    var id1 = parseInt($("#gidGraph").val())
    graph['id'] = id1
    

    var count = 0;
    $('#spn_selectGraph').children('input').each(function (index) {
        if (this.checked && count < 3) {
            graph["datasets"][count] = parseInt(this.value);
            count++;
        }
    });


    if (count > 1) {
        data["datasets"].push(JSON.parse(JSON.stringify(ds2)))
    }
    if (count >2) {
        data["datasets"].push(JSON.parse(JSON.stringify(ds3)))
    }
    

    var chartId = "myChart" + display_num;


    //set the size of the chart
    if ($("#g_size").val() == "small" ) {
        var txt = "<canvas id=" + chartId + " width=\"350\" height=\"300\"></canvas>";
    }
    if ($("#g_size").val() == "medium" ) {
        var txt = "<canvas id=" + chartId + " width=\"450\" height=\"400\"></canvas>";
    }
    if ($("#g_size").val() == "large" ) {
        var txt = "<canvas id=" + chartId + " width=\"550\" height=\"450\"></canvas>";
    }

    //get title value or else just call it chart 1 2 ..
    var title_text = $("#chartTitleInput").val()
    $("#chartTitleInput").val("")
    if (title_text == undefined ^ title_text == "") {title_text="Display " + display_num}
    var clabel = "<label for = '" + chartId + " '>" + title_text + "<br />"
    txt = clabel + txt + "</label>"



    //turn point smoothing on or off
    if ($("#curvesOption").val() == "yes" ) {
        options['bezierCurve'] = true;
    }
    else {
        options['bezierCurve'] = false;
    }


    //number of simultaneous points
    var points = $("#pointsSlider").val()


    //count contains number of points

    for (var i = 0; i < data['datasets'].length; i++) {
        for (var j = 0; j < points; j++) {
            data['datasets'][i]['data'][j] = 0;
            data['labels'][j] =""+ (j+1);
        }
        
    }
    


    

    //add canvas to the page
    $("#graphs").append(txt);
    var ctx = $("#" + chartId).get(0).getContext("2d");
    //add graph to array so that we can use it later
    //data and options are defined in chartSetup.js
    newchart = new Chart(ctx).Line(data, options)
    graph['chart_ref']= newchart


    //add to global array
    displays.push(graph)
    display_num++;
}

//live updating the demo chart TODO sprint 2

//slider
$("#pointsSlider").ionRangeSlider({
    min: 7,
    max: 50,
    from: 7,
});




//dynamically resize the demo chart.
$('#g_size').on('change', function() {
    demoChart.destroy()
    if (this.value == "small") {    
        demoCanvas.width = 350
        demoCanvas.height = 300
    }
    if (this.value == "medium") {    
         demoCanvas.width = 450
        demoCanvas.height = 400
    }
    if (this.value == "large") {    
         demoCanvas.width = 550
        demoCanvas.height = 500
    }
    
    ctx = demoCanvas.getContext("2d");
    var chart = new Chart(ctx).Line(demoData,demoOptions);
});


//need to get the options for the hardware and push to json
function setHardwareOptions(){
    var numNCs = $('#hardwareTable tr').length - 1;
    ncArray = []
    for (var i = 0; i < numNCs; i++) {
        ncObj = {}
        //fixed hopefully wow not....
        var row = $('#hardwareTable').find(' tbody tr:eq('+ i + ')').attr('id');
        var str = "#"+ row + " input:checkbox"
        console.log(row)
        var bid = row.substring(3,row.length)
        var number_id = parseInt(bid)
        //console.log(number_id)
        //console.log(bid)
        var include = $(str)[0].checked;
        var receive = $(str)[1].checked;
        var send = $(str)[2].checked;
        var filters = []
        if(include == true){
            ncObj['nc_id'] = number_id
            ncObj['forward_backbone'] = send
            ncObj['receive_backbone'] = receive
            ncObj['filters'] = filters
        }
        ncArray.push(ncObj);
     }

     if (ncArray.length < 1) {
        return;
     }else{
        jsonObj["NCs"] = ncArray;
        console.log(JSON.stringify(jsonObj))
     }

     
   hwSel = true;
   changeButtonState();
}

function Table(count,tableid){
    this.count = count;
    this.tableId = tableid;
    this.addData = function(timestamp,sourceaddr,priority,values){
        var text = "<tr><td>" + timestamp + "</td>" + "<td>" + sourceaddr + "</td><td>" + priority + "</td>"  
        for (var i = 0; i < count; i++) {
            text += "<td>" + values[i] + "</td>"    
        }
        text += "</tr>"


        $("#"+this.tableId).append(text)
    }
}