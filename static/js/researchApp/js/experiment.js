var displays = []
//return to the server the json
var jsonObj = {};
jsonObj["__setup__"]=true;
jsonObj["NCs"] = []
jsonObj["displays"] = []
var hwSel = false;
var graphSel = false;
var running = false;
var chartRefreshId;
var currentPGN = {};


function startExperiment(){
    running = true;

    //should send data to server
    var data = JSON.stringify(jsonObj);

    console.log(data)

    $.post('/research/start/', data, function(response) {
    // Do something with the request
    console.log("start exp got " + response);
    console.log(JSON.stringify(response))

        if (response["success"]==true) {
            chartRefreshId = setInterval(
                function(){
                    chartUpdate();
                },1000
            );
            changeButtonState();
        }else{
            alert("Something went wrong starting the experiment. Usually this happens when the hardware hasn't been started. Try refreshing the page and starting over. If that doesn't work contact the admin. Sorry for the trouble.")
            console.log("something went wrong starting the experiment")
        }

    }, 'json');


   

   
}


function stopExperiment() {
    if(chartRefreshId)
        clearInterval(chartRefreshId);

    var data = JSON.stringify("{'__close__':true}");
    $.post('/research/stop/', data, function(response) {
    // Do something with the request
    console.log("close experiment recieved: ");
    console.log(JSON.stringify(response))
    }, 'json');

    running = false;
    changeButtonState();
}

//uses the id of the graph to deliver graph updates
//need to modify for multiple data sets on a graph!
//http://api.jquery.com/jquery.getjson/
function chartUpdate () {
    console.log("updating chart");
    $.getJSON( "/research/get/", function(udata) {
        console.log("dumping json from chart update")
        console.log(JSON.stringify(udata))
        //find out who the data is for and deliver it!
        //check that this is data that we want:
       
       //exp is over
       if("end" in udata){
            clearInterval(chartRefreshId);
            running = false;
            changeButtonState();
            alert("The experiment is over")
        }

        if (!("graph_data" in udata)) {
            //this means that this is not graph data.
            console.log("Non-graph data recieved")
            //break at this point?
            return;
        };
        //find the id of the graph that we need
        //TODO exists checking on all this
        console.log("printing graph data")
        console.log(JSON.stringify(udata["graph_data"]))
        console.log("after graphdata")
        console.log("Udata.length is" + udata["graph_data"].length)

        for (var i = 0; i < udata["graph_data"].length; i++) {
            console.log("in i for loop " + i)
            var currId = udata["graph_data"][i]["pgn"]
            console.log("current pgn is" + currId)
            //find the chart that we need to deliver data to
            for (var q = 0; q < displays.length; q++) {
                //if true we need to deliver data to this graph
                console.log("display id is " + displays[q]["id"])
                console.log("currid is " + currId)
                if (displays[q]["id"]==currId) {
                    //deliver data to the graph or table!
                    if (displays[q]["type"] == "graph") {
                        
                        //create new points to send to chartjs
                        var newpts = []
                        for (var j = 0; j < displays[q]["datasets"].length; j++) {
                            
                            var currDS = displays[q]["datasets"][j]
                            console.log("currDS is" + currDS)
                            newpts[j] = udata["graph_data"][i]["spns"][currDS][1]
                        };


                        //set label
                        if('timestamp' in udata['graph_data'][i]){
                            var ts = udata['graph_data'][i]['timestamp']
                        }else{
                           var ts = ""
                        }
                            
                        //update 
                        displays[q]["chart_ref"].addData(newpts,ts);
                        displays[q]["chart_ref"].removeData();

                    }else{//this means its a table 
                        

                        var values = []
                        for (var j = 0; j < displays[q]["datasets"].length; j++) {
                            
                            var currDS = displays[q]["datasets"][j]
                            console.log("currDS is" + currDS)
                            values[j] = udata["graph_data"][i]["spns"][currDS][0]
                        };


                        
                        var timestamp = udata["graph_data"][i]["timestamp"]
                        var priority = udata["graph_data"][i]["priority"]
                        var sa = udata["graph_data"][i]["source"]
                        displays[q]["table_ref"].addData(timestamp,sa,priority,values)
                    


                    }
                };
            };
        };

  });

}

function injectRun() {
    var injectData = $('#injectData').val()

    if(injectData == null ^ injectData.length != 25){
        alert("Data not sent, please make sure it is in the correct form")
        return;
    }

    var jsonInject = {};
    jsonInject["__inject__"] = true;
    jsonInject["hex"]=injectData;

    var data = JSON.stringify(jsonInject);

    $.post('/research/inject/', data, function(response) {
        // Do something with the request
        console.log("response from data inject is " + response);

    }, 'json');

}

function requestRun() {
    var requestData = parseInt($('#requestPGN').val())
    var src = parseInt($('#srcnum').val())
    var dst = parseInt($('#dstnum').val())



    if(requestData == null ^ requestData < 0 ^ requestData > 100000 ^ src == null ^ src < 0 ^ src > 255^ dst == null ^ dst < 0 ^ dst > 255){
        alert("Data not sent, please make sure it is in the correct form")
        return;
    }

    var jsonRequest = {};
    jsonRequest["__request_pgn__"] = true;

    dstHex = dst.toString(16).toUpperCase();
    srcHex = src.toString(16).toUpperCase();

    while (srcHex.length < 2) {
        srcHex = "0" + srcHex;
    }
    while (dstHex.length < 2) {
        dstHex = "0" + dstHex;
    }


    var hexString = requestData.toString(16).toUpperCase();
    var p1 = hexString.substring(0, 2);
    var p2 = hexString.substring(2, 4);
    hexString = "18EA" + srcHex + dstHex +"#" + p2 + p1 + "00" + "FFFFFFFFFF"

    console.log(hexString)

    jsonRequest["hex"]=hexString;

    var data = JSON.stringify(jsonRequest);

    $.post('/research/inject/', data, function(response) {
        // Do something with the request
        console.log("response from inject is " + response);
        console.log(JSON.stringify(response))
    }, 'json');

}

