window.onload = startLoad()

var totalTime
var remainingTime
var intervalID;
function startLoad () {
	


	totalTime = parseInt($("#totalTime").html())
	remainingTime = parseInt($("#remainingTime").html())

	timeUpdate()	
	
	intervalID = setInterval(
    function(){
            timeUpdate();
        },1000
    );
}

function timeUpdate (argument) {
	if (remainingTime == 0) {
		window.clearInterval(intervalID);
		return
	};

	remainingTime = remainingTime - 1;
	var hours = Math.floor(remainingTime / 3600);
	var minutes = Math.floor(remainingTime / 60);
	var seconds = remainingTime % 60;
	var ts = hours + ":" + minutes + ":" + seconds;


	var percent = Math.floor((remainingTime / totalTime)*100);


	$('#timebar').css('width', percent +'%');
	
	var color
	if (percent>90) {
		color = "green";
	}else{
		if (percent>30) {
			color = "aqua";
		}else{
			if (percent>15) {
				color = "yellow";
			}else{
				color="red"
			}
			
		}
	}

	color = "progress-bar-" + color
	txt = "progress-bar"
	$("#timebar").removeClass("").addClass(txt + " " +color)

	$("#timetext").empty();
	$("#timetext").append("Time Remaining: " + ts);


}

function changeButtonState(){
    if (hwSel == true && graphSel == false && running == false) {
        $('#graphbtn').removeAttr('disabled');
        return
    }
    if (hwSel == true && graphSel == true && running == false) {
        $('#startbtn').removeAttr('disabled');
        return
    }
    if (hwSel == true && graphSel == true && running == true) {
        $('#startbtn').attr('disabled', 'disabled');
        $('#hwbtn').attr('disabled', 'disabled');
        $('#graphbtn').attr('disabled', 'disabled');
        $('#stopbtn').removeAttr('disabled');
        return
    }
    if (running==true) {
        $('#injectbtn').removeAttr('disabled');
        requestRun
    }

    $('#hwbtn').attr('disabled', 'disabled');
    $('#hwbtn').removeAttr('disabled');

    $('#graphbtn').attr('disabled', 'disabled');
    $('#graphbtn').removeAttr('disabled');

    $('#startbtn').attr('disabled', 'disabled');
    $('#startbtn').removeAttr('disabled');

    $('#stopbtn').attr('disabled', 'disabled');
    $('#stopbtn').removeAttr('disabled');

    hwSel == false
    graphSel == false
    running == false
}




//security!
//needed so that our ajax doesn't get killed by django
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
//please let my ajax in!
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});