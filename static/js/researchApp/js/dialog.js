function CustomAlert(){
    this.render = function(dialog,id){
        var winW = window.innerWidth;
        var winH = window.innerHeight;
        var dialogoverlay = document.getElementById('dialogoverlay');
        var dialogbox = document.getElementById('dialogbox');
        dialogoverlay.style.display = "block";
        dialogoverlay.style.height = winH+"px";
        dialogbox.style.left = (winW/2) - (550 * .5)+"px";
        dialogbox.style.top = "100px";
        dialogbox.style.display = "block";
        document.getElementById('dialogboxhead').innerHTML = "Sorry!";
        document.getElementById('dialogboxbody').innerHTML = dialog;
        $('#dialogboxfoot').html('<button class="btn btn-primary" onclick="Alert.no()">Close</button>');
//         document.getElementById('dialogboxfoot').innerHTML = '<button class="btn btn-primary" onmousedown="Alert.no()">Ok</button>';
    }
	this.no = function(){
		document.getElementById('dialogbox').style.display = "none";
		document.getElementById('dialogoverlay').style.display = "none";
	}
//	this.yes = function(id){
//        $.ajax({
//            url: "/management/users/",
//            method:"POST",
//            data: 'user_id='+id+'&type=delete',
//            success: function(result){
//               window.location = window.location;
//               // $("#users").html(result);
//            },
//        });
//	    document.getElementById('dialogbox').style.display = "none";
//	    document.getElementById('dialogoverlay').style.display = "none";
//        };
}
var Alert = new CustomAlert();
