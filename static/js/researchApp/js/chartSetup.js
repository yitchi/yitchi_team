//demo graph options
var demoCtx;
var demoData;
var demoOptions;
var demoChart;
var demoCanvas;

var template_data = {
            labels: ["1", "2", "3", "4", "5", "6", "7"],
            datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [65, 59, 80, 81, 56, 55, 40]
            }
            ]
        };
var ds2 = {
                label: "My Second dataset",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [28, 48, 40, 19, 86, 27, 90]
            }
var ds3 = {
                label: "My Third dataset",
                fillColor: "rgba(52,247,189,0.2)",
                strokeColor: "rgba(52,247,189,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [35, 70, 20, 50, 50, 35, 50]
            }        
var template_options = {
    ///Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines : true,
    //String - Colour of the grid lines
    scaleGridLineColor : "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth : 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve : true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension : 0.4,
    //Boolean - Whether to show a dot for each point
    pointDot : true,
    //Number - Radius of each point dot in pixels
    pointDotRadius : 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth : 1,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius : 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke : true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth : 2,
    //Boolean - Whether to fill the dataset with a colour
    datasetFill : true,
    //String - A legend template
    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
};


//for demo chart
$('#demoChart').on('shown.bs.modal',function(event){
    // Chart initialisieren
    var modal = $(this);
    var canvas = modal.find('.modal-body canvas');
    var ctx = canvas[0].getContext("2d");
    demoCanvas = canvas[0]
    demoData = JSON.parse(JSON.stringify(template_data));
    demoOptions = JSON.parse(JSON.stringify(template_options));
    //demoOptions['responsive'] = true
    demoOptions['maintainAspectRation'] = false
    demoOptions['animation'] = false;
    var chart = new Chart(ctx).Line(demoData,demoOptions);
    demoChart = chart;
}).on('hidden.bs.modal',function(event){
    // reset canvas size
    var modal = $(this);
    var canvas = modal.find('.modal-body canvas');
    //canvas.attr('width','568px').attr('height','300px');
    // destroy modal
    $(this).data('bs.modal', null);
});




/*window.onbeforeunload = function() {
        return "Please be aware that if you leave your graphs will be deleted";
}*/

function showCreateChart(){
    $('#tableSelect').hide();
    $('#chartSelect').show();
    $('#createChartBtn').addClass('active');
    $('#createTableBtn').removeClass('active');
 }

function showCreateTable(){
    $('#chartSelect').hide();
    $('#tableSelect').show();
    $('#createChartBtn').removeClass('active');
    $('#createTableBtn').addClass('active');
 }

function loadSelectOptionsChart(){
    //needs to load the possible spns for a certain pgn
    //show checkbox's to display on the graph

    var pgn = parseInt($("#gidGraph").val())
    console.log("pgn was "+ pgn)

    if (isNaN(pgn) ^ pgn < 0) {
        alert("Sorry that is an invalid PGN");
        return;
    }


    var testme = {}
    testme["__meta_pgn__"] = true;
    testme["pgn"] = pgn;

   //should send data to server
    var data = JSON.stringify(testme);


    $.post('/research/meta/', data, function(response) {
        // Do something with the request
        if('error' in response && response["error"]==true){
            alert("Looks like we don't support that PGN, try another one")
            $( "#spn_selectGraph" ).empty();
            return;
        }


        currentPGN = JSON.parse(JSON.stringify(response));

        $( "#spn_selectGraph" ).empty();
        if ('__meta_spn__' in response) {
            for (var i = 0; i < response["spns"].length; i++) {
                $('#spn_selectGraph').append('<input type="checkbox" name="spn" value=' + response["spns"][i] + ' />' + response["labels"][i] + '<br>');
            }
        }

        //console.log("meta " + response);
        //console.log(JSON.stringify(response))
    }, 'json');


}


function loadSelectOptionsTable(){
    //needs to load the possible spns for a certain pgn
    //show checkbox's to display on the graph

    var pgn = parseInt($("#gidTable").val())
    console.log("pgn was "+ pgn)

    if (isNaN(pgn) ^ pgn < 0) {
        alert("Sorry that is an invalid PGN");
        return;
    }


    var testme = {}
    testme["__meta_pgn__"] = true;
    testme["pgn"] = pgn;

   //should send data to server
    var data = JSON.stringify(testme);


    $.post('/research/meta/', data, function(response) {
        console.log(JSON.stringify(response))
    // Do something with the request
    if('error' in response && response["error"]==true){
        alert("Looks like we don't support that PGN, try another one")
        $( "#spn_selectTable" ).empty();
        return;
    }

    currentPGN = JSON.parse(JSON.stringify(response));

    $( "#spn_selectTable" ).empty();
    if ('__meta_spn__' in response) {
        for (var i = 0; i < response["spns"].length; i++) {
            $('#spn_selectTable').append('<input type="checkbox" name="spn" value=' + response["spns"][i] + ' />' + response["labels"][i] + '<br>');
        }
    }
    
    }, 'json');

    
}

$(window).on('beforeunload', function() {
    return "Are you sure you want to refresh? You will lose any data on the graphs and tables"
});