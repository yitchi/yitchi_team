import os, sys
path = os.path.dirname(os.path.dirname(__file__))
if path not in sys.path:
	sys.path.append(path)
os.environ['DJANGO_SETTINGS_MODULE'] = 'yitchi_team.settings'
import django.core.wsgi
application = django.core.wsgi.get_wsgi_application()
